import luigi
import radb
import ra2mr
import os

tmp_files = ["tmp1.tmp", "tmp2.tmp", "tmp3.tmp", "tmp4.tmp", "tmp5.tmp", "tmp6.tmp", "tmp7.tmp"]
for file in tmp_files:
	if os.path.isfile(file):
		os.remove(file)

raquery = radb.parse.one_statement_from_string("Person \join_{Person.name = Eats.name} Eats \join_{Eats.pizza = Serves.pizza} \select_{price=8}Serves;")
task = ra2mr.task_factory(raquery, env=ra2mr.ExecEnv.LOCAL)
luigi.build([task], local_scheduler=True)

f = task.output().open("r")
for line in f:
	print(line)
f.close()
