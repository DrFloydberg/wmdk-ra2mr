# sql2ra
#
# Written by Adrian Hammes
# for course WMDK
#
# version v0.1.0
# 2019-05-01
#

import radb
import radb.ast
import radb.parse
import sqlparse

# SQL parsing functions
def parseSQLTables(tokens: list) -> list:
	identifiers = []
	identifier_section = False

	for t in tokens:
		token: sqlparse.sql.Token = t
		
		# Find the start of the section with table identifiers
		if token.ttype is sqlparse.tokens.Keyword and token.value.lower() == "from":
			identifier_section = True
		
		# If identifier section started, read them out one by one
		if identifier_section:
			if isinstance(token, sqlparse.sql.Identifier):
				identifiers.append(token.value)
			elif isinstance(token, sqlparse.sql.IdentifierList):
				for i in token.get_identifiers():
					identifier: sqlparse.sql.Identifier = i
					identifiers.append(identifier.value)
			else:
				pass
		
	return identifiers

def parseSQLColumns(tokens: list) -> list:
	identifiers = []
	identifier_section = False

	for t in tokens:
		token: sqlparse.sql.Token = t
		
		# Find the start of the section with column identifiers
		if token.ttype is sqlparse.tokens.DML and token.value.lower() == "select":
			identifier_section = True

		# Section of column identifiers must have stopped
		if token.ttype is sqlparse.tokens.Keyword and token.value.lower() not in ["select", "distinct"]:
			identifier_section = False
		
		# If identifier section started, read them out one by one
		if identifier_section:
			if isinstance(token, sqlparse.sql.Identifier) and not token.is_wildcard():
				identifiers.append(token.value)
			elif isinstance(token, sqlparse.sql.IdentifierList):
				for i in token.get_identifiers():
					identifier: sqlparse.sql.Identifier = i
					identifiers.append(identifier.value)
			else:
				pass
		
	return identifiers

def parseSQLWhereClause(tokens: list) -> str:
	for t in tokens:
		token: sqlparse.sql.Token = t

		# unfortunately it always starts with 'where' which has to be removed
		if isinstance(token, sqlparse.sql.Where) and token.value.lower().startswith("where"):
			args = token.value.split()
			return " ".join(args[1:]).strip().replace(";", "")
			
	return ""

def parseSQL(tokens):
	tables = parseSQLTables(tokens)
	columns = parseSQLColumns(tokens)
	conditions = parseSQLWhereClause(tokens)

	return tables, columns, conditions

# Main function for handling translation of parsed sql statements into relational algebra statements
def translate(statement: sqlparse.sql.Statement):
	tables, columns, conditions = parseSQL(statement.tokens)
	ra = algebra(tables, columns, conditions)

	return radb.parse.one_statement_from_string(ra)

# Relational algebra compiling functions
def algebra(tables: list, columns: list, conditions: str):
	# First compile relations
	ra = algebraRelations(tables)
	
	selections = algebraSelections(conditions)
	if len(selections):
		ra = selections + "(" + ra + ")"

	projections = algebraProjections(columns)
	if len(projections):
		ra = projections + "(" + ra + ")"

	return ra + ";"

def algebraSelections(conditions: str):
	if len(conditions):
		return "\\select_{" + conditions + "}"
	else:
		return ""

def algebraRelations(tables: list):
	# almost recursive ;-)
	count = len(tables)
	if count == 1:
		return algebraRelation(tables[0])
	elif count == 2:
		return algebraRelation(tables[0]) + " \\cross " + algebraRelation(tables[1])
	else:
		return "(" + algebraRelations(tables[0:-1]) + ") \\cross " + algebraRelation(tables[-1])

def algebraRelation(table: str):
	args = table.split()

	# if two args, then a renaming has to be performed
	if len(args) > 1:
		return "\\rename_{" + args[1] + ": *} " + args[0]
	else:
		return args[0]

def algebraProjections(columns: list):
	if len(columns):
		return "\\project_{" + ", ".join(columns) + "}"
	else:
		return ""
