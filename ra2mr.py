
from enum import Enum
import json
import luigi
import luigi.contrib.hadoop
import luigi.contrib.hdfs
from luigi.mock import MockTarget
import radb
import radb.ast
import radb.parse

'''
Control where the input data comes from, and where output data should go.
'''
class ExecEnv(Enum):
    LOCAL = 1   # read/write local files
    HDFS = 2    # read/write HDFS
    MOCK = 3    # read/write mock data to an in-memory file system.

'''
Switches between different execution environments and file systems.
'''
class OutputMixin(luigi.Task):
    exec_environment = luigi.EnumParameter(enum=ExecEnv, default=ExecEnv.HDFS)
    
    def get_output(self, fn):
        if self.exec_environment == ExecEnv.HDFS:
            return luigi.contrib.hdfs.HdfsTarget(fn)
        elif self.exec_environment == ExecEnv.MOCK:
            return MockTarget(fn)
        else:
            return luigi.LocalTarget(fn)


class InputData(OutputMixin):
    filename = luigi.Parameter()

    def output(self):
        return self.get_output(self.filename)


'''
Counts the number of steps / luigi tasks that we need for evaluating this query.
'''
def count_steps(raquery):
    assert(isinstance(raquery, radb.ast.Node))

    if (isinstance(raquery, radb.ast.Select) or isinstance(raquery,radb.ast.Project) or
        isinstance(raquery,radb.ast.Rename)):
        return 1 + count_steps(raquery.inputs[0])

    elif isinstance(raquery, radb.ast.Join):
        return 1 + count_steps(raquery.inputs[0]) + count_steps(raquery.inputs[1])

    elif isinstance(raquery, radb.ast.RelRef):
        return 1

    else:
        raise Exception("count_steps: Cannot handle operator " + str(type(raquery)) + ".")


class RelAlgQueryTask(luigi.contrib.hadoop.JobTask, OutputMixin):
    '''
    Each physical operator knows its (partial) query string.
    As a string, the value of this parameter can be searialized
    and shipped to the data node in the Hadoop cluster.
    '''
    querystring = luigi.Parameter()

    '''
    Each physical operator within a query has its own step-id.
    This is used to rename the temporary files for exhanging
    data between chained MapReduce jobs.
    '''
    step = luigi.IntParameter(default=1)

    '''
    In HDFS, we call the folders for temporary data tmp1, tmp2, ...
    In the local or mock file system, we call the files tmp1.tmp...
    '''
    def output(self):
        if self.exec_environment == ExecEnv.HDFS:
            filename = "tmp" + str(self.step)
        else:
            filename = "tmp" + str(self.step) + ".tmp"
        return self.get_output(filename)


'''
Given the radb-string representation of a relational algebra query,
this produces a tree of luigi tasks with the physical query operators.
'''
def task_factory(raquery, step=1, env=ExecEnv.HDFS):
    assert(isinstance(raquery, radb.ast.Node))
    
    if isinstance(raquery, radb.ast.Select):
        return SelectTask(querystring=str(raquery) + ";", step=step, exec_environment=env)

    elif isinstance(raquery, radb.ast.RelRef):
        filename = raquery.rel + ".json"
        return InputData(filename=filename, exec_environment=env)

    elif isinstance(raquery, radb.ast.Join):
        return JoinTask(querystring=str(raquery) + ";", step=step, exec_environment=env)

    elif isinstance(raquery, radb.ast.Project):
        return ProjectTask(querystring=str(raquery) + ";", step=step, exec_environment=env)

    elif isinstance(raquery, radb.ast.Rename):
        return RenameTask(querystring=str(raquery) + ";", step=step, exec_environment=env)
                          
    else:
        # We will not evaluate the Cross product on Hadoop, too expensive.
        raise Exception("Operator " + str(type(raquery)) + " not implemented (yet).")
    

class JoinTask(RelAlgQueryTask):

    def requires(self):
        raquery = radb.parse.one_statement_from_string(self.querystring)
        assert(isinstance(raquery, radb.ast.Join))
      
        task1 = task_factory(raquery.inputs[0], step=self.step + 1, env=self.exec_environment)
        task2 = task_factory(raquery.inputs[1], step=self.step + count_steps(raquery.inputs[0]) + 1, env=self.exec_environment)

        return [task1, task2]

    
    def mapper(self, line):
        relation, tuple = line.split('\t')
        json_tuple = json.loads(tuple)
        
        raquery = radb.parse.one_statement_from_string(self.querystring)
        condition: radb.ast.ValExprBinaryOp = raquery.cond

        ''' ...................... fill in your code below ........................'''
        # Condition analysis first
        if condition.op == radb.parse.RAParser.AND:
            A = condition.inputs[0].inputs[0].name
            A_rel = condition.inputs[0].inputs[0].rel
            B = condition.inputs[0].inputs[1].name
            B_rel = condition.inputs[0].inputs[1].rel
            C = condition.inputs[1].inputs[0].name
            C_rel = condition.inputs[1].inputs[0].rel
            D = condition.inputs[1].inputs[1].name
            D_rel = condition.inputs[1].inputs[1].rel

            # Yield if common columns have been found
            if A == B and C == D:
                A_full = A_rel + '.' + A
                B_full = B_rel + '.' + B
                C_full = C_rel + '.' + C
                D_full = D_rel + '.' + D

                key_1 = ""
                key_2 = ""

                if A_full in json_tuple:
                    key_1 = str(json_tuple[A_full])
                else:
                    key_1 = str(json_tuple[B_full])

                if C_full in json_tuple:
                    key_2 = str(json_tuple[C_full])
                else:
                    key_2 = str(json_tuple[D_full])

                value = relation + '\t' + tuple
                yield(key_1 + ',' + key_2, value)
        else:
            A = condition.inputs[0].name
            A_rel = condition.inputs[0].rel
            B = condition.inputs[1].name
            B_rel = condition.inputs[1].rel
            # Yield if common column has been found
            if A == B:
                A_full = A_rel + '.' + A
                B_full = B_rel + '.' + B
                key = ""
                if A_full in json_tuple:
                    key = str(json_tuple[A_full])
                else:
                    key = str(json_tuple[B_full])
                value = relation + '\t' + tuple
                yield(key, value)


        ''' ...................... fill in your code above ........................'''


    def reducer(self, key, values):
        raquery = radb.parse.one_statement_from_string(self.querystring)
        
        ''' ...................... fill in your code below ........................'''
        # List multiple entries from one relaton in one dictionary's entry
        vals = list(values)
        vals.sort()
        base_relations = {}
        for val in vals:
            rel, json_txt = val.split('\t')
            if rel not in base_relations:
                base_relations[rel] = []
            base_relations[rel].append(json_txt)
        
        # Construct some objects
        objs = []
        
        # There are two cases:
        # 1. One relation --> no matching tuple in other relation --> abort
        # 2. Two relations --> some matching tuples --> dump objects to json-output
        x = list(base_relations.keys())
        if len(x) == 2:
            relations_A = base_relations[x[0]]
            relations_B = base_relations[x[1]]
                
            for rel_A in relations_A:
                for rel_B in relations_B:
                    A = json.loads(rel_A)
                    B = json.loads(rel_B)

                    obj = {**A, **B} # Merge dictionaries since Python 3.5
                    objs.append(obj)


            # Yield
            for obj in objs:
                output = json.dumps(obj)
                yield(','.join(x), output)

        ''' ...................... fill in your code above ........................'''   


class SelectTask(RelAlgQueryTask):

    def requires(self):
        raquery = radb.parse.one_statement_from_string(self.querystring)
        assert(isinstance(raquery, radb.ast.Select))
        
        return [task_factory(raquery.inputs[0], step=self.step + 1, env=self.exec_environment)]


    # Helper function splits a binary comparison into it's parameter and value
    def conditionAnalysis(self, condition: radb.ast.ValExprBinaryOp, relation: str):
        A = condition.inputs[0]
        B = condition.inputs[1]

        param = ""
        value = ""

        if isinstance(A, radb.ast.AttrRef):
            param = str(A)
            value = str(B).replace('\'', '')
        else:
            param = str(B)
            value = str(A).replace('\'', '')

        if relation not in param:
            param = relation + "." + param

        return param, value

    def mapper(self, line):
        relation, tuple = line.split('\t')
        json_tuple = json.loads(tuple)

        condition: radb.ast.ValExprBinaryOp = radb.parse.one_statement_from_string(self.querystring).cond
        
        ''' ...................... fill in your code below ........................'''
        # Test if condition is concatinated
        do_yield = False

        if condition.op == radb.parse.RAParser.AND:
            A_param, A_value = self.conditionAnalysis(condition.inputs[0], relation)
            B_param, B_value = self.conditionAnalysis(condition.inputs[1], relation)

            ap = str(json_tuple[A_param])
            av = str(A_value)

            bp = str(json_tuple[B_param])
            bv = str(B_value)
            if ap == av and bp == bv:
                do_yield = True
        else:
            param, value = self.conditionAnalysis(condition, relation)
            if (str(json_tuple[param]) == str(value)):
                do_yield = True

        # Yield if tests succeeded
        if (do_yield):
            yield(relation, tuple)
        ''' ...................... fill in your code above ........................'''


class RenameTask(RelAlgQueryTask):

    def requires(self):
        raquery = radb.parse.one_statement_from_string(self.querystring)
        assert(isinstance(raquery, radb.ast.Rename))

        return [task_factory(raquery.inputs[0], step=self.step + 1, env=self.exec_environment)]


    def mapper(self, line):
        relation, tuple = line.split('\t')
        json_tuple = json.loads(tuple)

        raquery: radb.ast.Rename = radb.parse.one_statement_from_string(self.querystring)
        
        ''' ...................... fill in your code below ........................'''
        # Get old and new name
        new = raquery.relname
        old = raquery.inputs[0].rel

        # Replace in dictionary for every entry
        for entry in json_tuple:
            rel, param = entry.split('.')
            if old in rel:
                json_tuple[new + '.' + param] = json_tuple.pop(entry)

        # Yield
        output = json.dumps(json_tuple)
        yield(new, output)
        
        ''' ...................... fill in your code above ........................'''


class ProjectTask(RelAlgQueryTask):

    def requires(self):
        raquery = radb.parse.one_statement_from_string(self.querystring)
        assert(isinstance(raquery, radb.ast.Project))

        return [task_factory(raquery.inputs[0], step=self.step + 1, env=self.exec_environment)]    


    def mapper(self, line):
        relations, tuple = line.split('\t')
        json_tuple = json.loads(tuple)

        attrs = radb.parse.one_statement_from_string(self.querystring).attrs

        ''' ...................... fill in your code below ........................'''
        
        # Extend attrs with relation name if necessary
        columns = []
        for attr in attrs:
            attrname = str(attr)
            relations_splitted = relations.split(',')
            if len(relations_splitted) == 1 and relations not in attrname:
                columns.append(relations + '.' + attrname)
            else:
                columns.append(attrname)

        # Now filter columns from tuple
        to_delete = []
        for entry in json_tuple:
            if entry not in columns:
                to_delete.append(entry)

        for entry in to_delete:
            json_tuple.pop(entry)

        # Yield
        output = json.dumps(json_tuple)
        yield(relations, output)
        
        ''' ...................... fill in your code above ........................'''


    def reducer(self, key, values):

        ''' ...................... fill in your code below ........................'''
        # Remove duplicates
        vals = list(values)
        vals = list(set(vals)) # dirty, really stupid actually

        for value in vals:
            yield(key, value)

        ''' ...................... fill in your code above ........................'''
        
        
if __name__ == '__main__':
    luigi.run()
